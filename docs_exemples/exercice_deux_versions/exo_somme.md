---
author: Votre nom
title: Exercice avec du code caché et deux versions
tags:
  - liste/tableau
---



La liste `nombres` se trouve dans du code caché.

Compléter la fonction `somme` qui prend en paramètre une liste de nombres `ma_liste`, et renvoie la somme des nombres de cette liste.

=== "Version vide"
    {{ IDE('exo_vide') }}

=== "Version à compléter"
    {{ IDE('exo_trous') }}





